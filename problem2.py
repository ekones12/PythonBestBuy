# -*- coding: utf-8 -*-
#!/usr/bin/env python

import sys

x=sys.argv[1]

linelist=[]
BulletList=[]
c = open(x, "r")

lines = c.readlines()
c.close()

for n in lines :

    BulletList += [n.split(" ")]#this loop for create 2 dimensional array list
for i in range(0,len(BulletList)-1) :#this loop for remove /n from taxe value
    BulletList[i][2] = BulletList[i][2][:-1]

for i in range(0, len(BulletList)):#this loop for conversion to float from str
    BulletList[i] = [float(a) for a in BulletList[i]]


def calculateTotalCost(list):#this fuction for statement to calculate the total cost of each house
    ResultList = []
    for i in list:
        a = i[0]
        b = i[1] * 10
        c = i[0] * 10 * i[2]
        totalcost = a + b + c
        ResultList.append(totalcost)
    return ResultList


def displayCosts(list):#this function for display the total cost of each house
    displayList=calculateTotalCost(list)
    print ("The total cost of each house :")
    for i in range(len(displayList)):
        print ("{}. house's total cost is {}".format(i+1,displayList[i]))


def selectBestBuy(list):#this function display which one is the best buy
    BestBuyList=calculateTotalCost(list)
    BestBuyList2=BestBuyList[:]
    for i in range(0,len(BestBuyList)-1):#this loop for compares items in order of small to large
        while (i > 0) and (BestBuyList[i-1] > BestBuyList[i]):
            BestBuyList[i-1] , BestBuyList[i] = BestBuyList [i] , BestBuyList[i-1]
            i -= 1
    a = BestBuyList2.index(BestBuyList[0])

    print("you should select {}. house whose total cost is {}".format(a+1, BestBuyList[0]))
displayCosts(BulletList)
selectBestBuy(BulletList)












